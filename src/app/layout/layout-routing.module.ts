import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LayoutComponent } from './components/layout/layout.component';

const routes: Routes = [
  {path: '', component: LayoutComponent, children: [
    {
      path: 'dashboard', component: DashboardComponent
    },
    {
      path: '', redirectTo: 'dashboard', pathMatch: 'full'
    },
    {path: 'products', loadChildren: () => import('../products/products.module').then(m => m.ProductsModule)},
    {path: 'orders', loadChildren: () => import('../orders/orders.module').then(m => m.OrdersModule)}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
