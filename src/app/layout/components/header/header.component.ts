import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() open = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  abrirMenu() {
    this.open.emit('Abrir Menu.');
  }

}
