import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private firestore: AngularFirestore
  ) { }

  newProduct(product: any) {
    const productCollection = this.firestore.collection('products');
    return productCollection.add(product);
  }

  getProducts() {
    return this.firestore.collection('products').valueChanges({ idField: 'id' });
  }
}
