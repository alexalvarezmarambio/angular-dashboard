import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormArray, Validators} from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
import { v4 as uuidv4 }  from 'uuid';
import {Observable} from 'rxjs';
import { finalize } from 'rxjs/operators';
import {ProductService} from '../../services/product.service';
import {UtilityService} from 'src/app/shared/services/utility.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styles: [
  ],
  providers: [UtilityService]
})
export class NewProductComponent implements OnInit {

  isImage = true;
  isImages = true;
  image: any;
  imagesEvent: any;

  imagePercent!: Observable<any>;
  imagesPercent!: Observable<any>;

  formProduct = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    price: ['', [Validators.required, Validators.min(1)]],
    stock: ['', [Validators.required, Validators.min(0)]],
    category: [['CAJA'], Validators.required],
    width: ['', [Validators.required, Validators.min(2)]],
    height: ['', [Validators.required, Validators.min(2)]],
    length: ['', [Validators.required, Validators.min(2)]],
    image: ['', Validators.required],
    images: this.fb.array([], Validators.required)
  });

  get images() {
    return this.formProduct.get('images') as FormArray;
  }

  constructor(
    private fb: FormBuilder,
    private storage: AngularFireStorage,
    public productService: ProductService,
    public utils: UtilityService,
    private dialogRef: MatDialogRef<NewProductComponent>
  ) { }

  ngOnInit(): void {}

  seeForm() {
    console.log(this.formProduct.value);
  }

  async newProduct() {
    try {
      await this.productService.newProduct(this.formProduct.value);
      this.dialogRef.close(true);
    } catch (error) {
      this.utils.mostrarSnackBar(error.message);
    }
  }

  changeImage(event: any) {

    if (0 in event.target.files) {
      this.image = event;
      this.isImage = false;
      return;
    }

    this.isImage = true;

  }

  uploadImage() {
    this.upload(this.image.target.files[0], 'single');
  }

  upload(image: File, form: string) {
    let downloadUrl: Observable<string>;

    const file = image;
    const filePath = 'shop/' + uuidv4();
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    if (form === 'single') {
      this.imagePercent = task.percentageChanges();
    }

    if (form === 'multi') {
      this.imagesPercent = task.percentageChanges();
    }

    task.snapshotChanges().pipe(
      finalize( () => {
        downloadUrl = fileRef.getDownloadURL();
        downloadUrl.subscribe(url => {
          if (form === 'single') {
            this.formProduct.controls.image.setValue(url);
          } else {
            this.images.push(this.fb.control(url));
          }
        });
      })
    ).subscribe();

  }

  changeImages(event: any) {

    if (0 in event.target.files) {
      this.imagesEvent = event;
      this.isImages = false;
      return;
    }

    this.isImages = true;
  }

  uploadImages() {
    const files: FileList = this.imagesEvent.target.files;
    for (let i = 0; i < files.length; i++) {
      this.upload(files[i], 'multi');
    }
      
  }

}
