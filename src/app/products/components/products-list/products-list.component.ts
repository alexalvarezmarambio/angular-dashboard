import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {ConfirmComponent} from 'src/app/shared/components/confirm/confirm.component';
import {AuthService} from 'src/app/shared/services/auth.service';
import {UtilityService} from 'src/app/shared/services/utility.service';
import {ProductService} from '../../services/product.service';
import {NewProductComponent} from '../new-product/new-product.component';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styles: [],
  providers: [UtilityService]
})
export class ProductsListComponent implements OnInit {

  products!: Observable<any>;

  constructor(
    public productService: ProductService,
    public utils: UtilityService,
    public auth: AuthService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.products = this.productService.getProducts();
  }

  async newProduct() {
    try {
      const dialogRef = this.dialog.open(NewProductComponent, {
        disableClose: true
      });

      const respuesta = await dialogRef.afterClosed().toPromise();
      if (respuesta) {
        const snackRef = this.utils.mostrarSnackBar('Product Created.');
        await snackRef.afterDismissed().toPromise();
        // getProducts();
      }
    } catch (error) {
      this.utils.mostrarSnackBar(error.message);
    }
  }

  seeId(id: string) {
    console.log(id);
  }

}
