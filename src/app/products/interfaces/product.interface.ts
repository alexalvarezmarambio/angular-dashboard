export interface Product {
  id?: string;
  category: string[];
  description: string;
  height: number;
  image: string;
  images: string[];
  length: number;
  name: string;
  price: number;
  stock: number;
  width: number;
}
