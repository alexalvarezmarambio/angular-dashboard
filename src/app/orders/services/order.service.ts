import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {Order} from '../interfaces/order.interface';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(
    private afs: AngularFirestore
  ) { }

  getOrders(): Observable<any[]> {
    return this.afs.collection<Order[]>('orders', ref => ref.where('state', '!=', 'ENTREGA')).valueChanges({idField: 'id'});
  }

  getOrder(id: string): Observable<any> {
    return this.afs.doc<Order>(`orders/${id}`).valueChanges({idField: 'id'});
  }

  updateOrderState(id: string, state: string) {
    const documentRef = this.afs.doc(`orders/${id}`);
    return documentRef.update({state});
  }
}
