import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Order} from '../../interfaces/order.interface';
import {OrderService} from '../../services/order.service';

@Component({
  selector: 'app-list-orders',
  templateUrl: './list-orders.component.html',
  styles: [
  ]
})
export class ListOrdersComponent implements OnInit {

  orders!: Observable<Order[]>

  constructor(
    public orderService: OrderService
  ) { }

  ngOnInit(): void {
    this.getOrders();
  }

  getOrders() {
    this.orders = this.orderService.getOrders();
  }

}
