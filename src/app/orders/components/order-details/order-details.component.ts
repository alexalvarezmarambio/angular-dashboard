import { Component, OnInit } from '@angular/core';
import { AngularFirestoreDocument } from '@angular/fire/firestore';

import { ActivatedRoute } from '@angular/router';
import { Order } from '../../interfaces/order.interface';
import { Observable } from 'rxjs';
import { OrderService } from '../../services/order.service';


@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styles: [
  ]
})
export class OrderDetailsComponent implements OnInit {

  order!: Observable<Order>;
  id = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    public orderService: OrderService
  ) {}

  ngOnInit(): void {
    this.getOrder();
  }

  getOrder() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.order = this.orderService.getOrder(this.id);
    });
  }

  updateOrderState(state: string) {
    this.orderService.updateOrderState(this.id, state);
  }

}


