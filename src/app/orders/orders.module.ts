import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { ListOrdersComponent } from './components/list-orders/list-orders.component';
import {MaterialModule} from '../shared/material.module';
import { OrderDetailsComponent } from './components/order-details/order-details.component';


@NgModule({
  declarations: [ListOrdersComponent, OrderDetailsComponent],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    MaterialModule
  ]
})
export class OrdersModule { }
