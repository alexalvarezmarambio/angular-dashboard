import {ItemOrder} from './item-order.interface';
import firebase from 'firebase'

export interface Order {
  id?: string;
  address: string;
  date: firebase.firestore.Timestamp;
  delivery: string;
  email: string;
  lastUpdate: firebase.firestore.Timestamp;
  name: string;
  pay: string;
  phone: string;
  state: string;
  products: ItemOrder[];
}
