import {Product} from 'src/app/products/interfaces/product.interface';

export interface ItemOrder {
  product: Product;
  qty: number;
}
