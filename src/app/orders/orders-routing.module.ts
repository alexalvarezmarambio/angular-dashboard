import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListOrdersComponent} from './components/list-orders/list-orders.component';
import { OrderDetailsComponent } from './components/order-details/order-details.component';

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: ListOrdersComponent},
  {path: 'order/:id', component: OrderDetailsComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
